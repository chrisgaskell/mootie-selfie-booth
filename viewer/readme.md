Mac OsX Webserver
-------------------
To use the webserver on a mac to quickly spin up the viewer Angular app:

* cd /path/to/project/folder
* python -m SimpleHTTPServer 8080
* Open the following url in a broswer: http://localhost:8080
