angular.module("mootphoto", ['chieffancypants.loadingBar', 'ngAnimate'])

.config(function ( $httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    $httpProvider.defaults.headers.common = 'Content-Type: application/json';
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
})
.config(function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = true;
})

.controller("PhotoController", function($scope, $http, $interval) {

  $scope.latestPhotos = [];
  $scope.randomPhotos = [];
  $scope.host = 'http://192.168.2.2/';

  $scope.random = function(length){
    return Math.floor(Math.random() * length);
  };

  $scope.doRefresh = function() {
    $http({
      method: 'GET',
      url: $scope.host + 'api.php'
    }).then(function successCallback(response) {

	  $scope.latestPhotos = [];
	  $scope.randomPhotos = [];
	
      var data = response.data.reverse();
      $scope.photoCount = data.length + 1;

      var rand1 = $scope.random(response.data.length +1);
      var rand2 = $scope.random(response.data.length +1);
      var rand3 = $scope.random(response.data.length +1);
      var rand4 = $scope.random(response.data.length +1);
      console.log(rand1 + '-' + rand2 + '-' + rand3);

      $scope.latestPhotos.push(data[0]);
      $scope.latestPhotos.push(data[1]);
      $scope.latestPhotos.push(data[2]);
      $scope.latestPhotos.push(data[3]);

      if(data.length > 7){
        $scope.randomPhotos.push(data[rand1]);
        $scope.randomPhotos.push(data[rand2]);
        $scope.randomPhotos.push(data[rand3]);
        $scope.randomPhotos.push(data[rand4]);
      }

      console.log('API Success');

    }, function errorCallback(response) {
      console.log('API HTTP ERROR');
    });
  };

  $scope.doRefresh();
  $interval( function(){ $scope.doRefresh(); }, 10000);

});
