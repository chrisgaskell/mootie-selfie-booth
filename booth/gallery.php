<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="refresh" content="10">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<?php
    glob('/var/www/html/uploads/*.jpeg');
    $output = array();
    foreach (glob('/var/www/html/uploads/*.jpeg') as $file) {
        $array = explode("/", $file);
        unset($array[0]);
        unset($array[1]);
        unset($array[2]);
        unset($array[3]);
        $output[] = implode("/", $array);
    }
    $cnt = count($output);
    //print_r($output);
?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Mootie Gallery</h1>
            </div>

            <div class="col-lg-4 col-md-3 col-xs-6 thumb">
                <a class="thumbnail" href="<?php echo $output[$cnt-1]; ?>">
                    <img class="img-responsive" src="<?php echo $output[$cnt-1]; ?>" alt="<?php echo $output[$cnt-1]; ?>">
                </a>
            </div>
            <div class="col-lg-4 col-md-3 col-xs-6 thumb">
                <a class="thumbnail" href="<?php echo $output[$cnt-2]; ?>">
                    <img class="img-responsive" src="<?php echo $output[$cnt-2]; ?>" alt="<?php echo $output[$cnt-2]; ?>">
                </a>
            </div>
            <div class="col-lg-4 col-md-3 col-xs-6 thumb">
                <a class="thumbnail" href="<?php echo $output[$cnt-3]; ?>">
                    <img class="img-responsive" src="<?php echo $output[$cnt-3]; ?>" alt="<?php echo $output[$cnt-3]; ?>">
                </a>
            </div>
            <div class="col-lg-4 col-md-3 col-xs-6 thumb">
                <a class="thumbnail" href="<?php echo $output[rand(1,$cnt)]; ?>">
                    <img class="img-responsive" src="<?php echo $output[rand(4,$cnt)]; ?>" alt="<?php echo $output[rand(1,$cnt)]; ?>">
                </a>
            </div>
            <div class="col-lg-4 col-md-3 col-xs-6 thumb">
                <a class="thumbnail" href="<?php echo $output[rand(4,$cnt)]; ?>">
                    <img class="img-responsive" src="<?php echo $output[rand(4,$cnt)]; ?>" alt="<?php echo $output[rand(1,$cnt)]; ?>">
                </a>
            </div>
            <div class="col-lg-4 col-md-3 col-xs-6 thumb">
                <a class="thumbnail" href="<?php echo $output[rand(4,$cnt)]; ?>">
                    <img class="img-responsive" src="<?php echo $output[rand(4,$cnt)]; ?>" alt="<?php echo $output[rand(1,$cnt)]; ?>">
                </a>
            </div>
        </div>

        <hr>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>

