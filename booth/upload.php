<?php

function saveImage($base64img){
    define('UPLOAD_DIR', '/var/www/html/uploads/');
    $img = str_replace('data:image/jpeg;base64,', '',  urldecode($base64img));
    $data = base64_decode($img);
    $date = getdate();
    $file = UPLOAD_DIR . date('d-m-Y_H-i-s') . '.jpeg';
    file_put_contents($file, $data);
}

if(isset($_POST["photo"])) 
{
    saveImage(utf8_decode($_POST['photo'])); 
}
else
{
    http_response_code(404);
}
?>
