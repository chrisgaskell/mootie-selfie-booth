# Mootie Selfie Booth

A HTML5 web application that takes 4 photos and stiches them together into one folder. Images are stored within the /uploads/ directory. api.php is used for the viewer application to grab the details of the latest images. Viewer application to be setup on a different RaspberryPi. 

## RPi Setup Commands [Booth machine only]
This is not a shell script. Requires user interaction for the certificate setup. Initial setup requires Internet to install the dependancies. 

```sh
sudo su
echo "kiosk.local" > /etc/hostname
wget -qO - http://bintray.com/user/downloadSubjectPublicKey?username=bintray | sudo apt-key add -
echo "deb http://dl.bintray.com/kusti8/chromium-rpi jessie main" | sudo tee -a /etc/apt/sources.list
apt-get update
apt-get install vim apache2 php5 libapache2-mod-php5 iceweasel chromium-browser -y
rm -r /var/www/html/index.html
cd /var/www/html/
git clone https://gavinanders@bitbucket.org/chrisgaskell/mootie-selfie-booth.git
mkdir /var/www/html/uploads/
chmod a+wr /var/www/html/uploads/
cd /etc/apache2/
openssl req -x509 -nodes -days 1095 -newkey rsa:2048 -out server.crt -keyout server.key
a2enmod ssl
ln -s /etc/apache2/sites-available/default-ssl.conf /etc/apache2/sites-enabled/000-default-ssl.conf
vim /etc/apache2/sites-enabled/000-default-ssl.conf
service apache2 reload

echo '@lxpanel --profile LXDE-pi' > /home/pi/.config/lxsession/LXDE-pi/autostart
echo '@pcmanfm --desktop --profile LXDE-pi' >> /home/pi/.config/lxsession/LXDE-pi/autostart
echo '@xset s off' >> /home/pi/.config/lxsession/LXDE-pi/autostart
echo '@xset s noblank' >> /home/pi/.config/lxsession/LXDE-pi/autostart
echo '@xset -dpms' >> /home/pi/.config/lxsession/LXDE-pi/autostart
echo '@iceweasel https://localhost/' >> /home/pi/.config/lxsession/LXDE-pi/autostart
```

## RPi Setup Commands [Viewer machines only]
 ```sh
sudo su
echo "viewer.local" > /etc/hostname
wget -qO - http://bintray.com/user/downloadSubjectPublicKey?username=bintray | sudo apt-key add -
echo "deb http://dl.bintray.com/kusti8/chromium-rpi jessie main" | sudo tee -a /etc/apt/sources.list
apt-get update
apt-get install vim iceweasel chromium-browser -y

echo '@lxpanel --profile LXDE-pi' > /home/pi/.config/lxsession/LXDE-pi/autostart
echo '@pcmanfm --desktop --profile LXDE-pi' >> /home/pi/.config/lxsession/LXDE-pi/autostart
echo '@xset s off' >> /home/pi/.config/lxsession/LXDE-pi/autostart
echo '@xset s noblank' >> /home/pi/.config/lxsession/LXDE-pi/autostart
echo '@xset -dpms' >> /home/pi/.config/lxsession/LXDE-pi/autostart
echo '@iceweasel http://kiosk.local/viewer/' >> /home/pi/.config/lxsession/LXDE-pi/autostart
```

## Manual Setup
  - A wireless network is required with a static IP address for each of the machines. Configuration of the wireless as static IP can be set through the raspbien GUI
  - The certificate CA can be installed within FF to avoid certificate errors on load
  - To turn FF into a kiosk (most likely the last thing to do), R-Kisosk add-on needs to be installed - https://addons.mozilla.org/en-GB/firefox/addon/r-kiosk/

## Troubleshoot
#### Camera is not shown on the kiosk
  - Browser is loading the HTTP version of the site 
  - Camera is not install or working (check with ```lsusb``` and ```tail -f /var/log/messages``` whilst opening the  browser)
#### Browser not showing the viewer app 
 - Most likely pointing to the wrong IP address, or DNS resolution isnt working for kiosk.local
 - Set a static IP address in the code
 - 
#### Booth/Viewer not shown on the screen on boot
 - Wrong IP address or DNS resolution issues
 - The autoboot of FF is running before the RPi has been given a change to join the network. (change in Raspberry Pi Configuration Application - tick 'Wait for network')
#### RPi screen is off screen or has black boarders
 - Overscan settings need tweaking in /boot/config.txt (see internet for further guidance)
 
## ToDo
 - Add a printer?
 - Resolve the intermittent black canvas for the web cam (most likely needs refreshing every minute)