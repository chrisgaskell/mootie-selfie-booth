<?php
    header('Content-Type: application/json');
    header("Access-Control-Allow-Origin: *");

    glob('/var/www/html/uploads/*.jpeg');
    $output = array();
    foreach (glob('/var/www/html/uploads/*.jpeg') as $file) {
        $array = explode("/", $file);
        unset($array[0]);
	unset($array[1]);
	unset($array[2]);
        unset($array[3]);
        $uri = implode("/", $array);        

        $element = array();
        $element['uri'] = $uri;
	$element['lastUpdated'] = filemtime($file);   

        $output[] = $element;
    }
    echo json_encode($output);
?>
