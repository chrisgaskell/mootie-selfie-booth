<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <script src="./js/jquery.js"></script>
    <title>Photo booth Kiosk</title>
</head>

<body>
<style>
.camera { 
   position: relative; 
   width: 100%; /* for IE 6 */
}
.counter { 
   position: absolute; 
   top: 150px; 
   left: 0; 
   width: 100%;
   font-size: 200px;
   text-align:center;
   margin: auto;
   color: white;
}

</style>
    
<img hidden id="profile" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADXCAIAAADQuomHAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAxBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMi1jMDAxIDYzLjEzOTQzOSwgMjAxMC8xMC8xMi0wODo0NTozMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MDQ4OTAxQzkwMDY3MTFFM0E4N0U4MzhGQkJBRDdENkEiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MDQ4OTAxQzgwMDY3MTFFM0E4N0U4MzhGQkJBRDdENkEiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9IkMwNzVFNTJEMDE1MzNBNDkxMDBCOEQ3QjAwQ0JBRjg1IiBzdFJlZjpkb2N1bWVudElEPSJDMDc1RTUyRDAxNTMzQTQ5MTAwQjhEN0IwMENCQUY4NSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pi/+HGYAAC6vSURBVHja7F37cxXH0R3EAnohIZBkCSPxjIlTLnAZ24krqVSl8sfkj0zFedgkTlIuxyQ2BCQMAoQAPdADvfBnfwc1dIZ59PTs494rof1BdbV37727O2e7T5/u6Sl+97vfmd2zHThwgP/++OOP9h77X/+F80H/MOcnatnolHj74YcfhMPsv/5++5Kdr+3Mrdh1qAqCzP/X3uPvDB5fO7A032yjPLmfnh9/fwdCbTcBS8CT/MKGl2+0Wgasrq4uwVzZoHFe+LjpfKNV7DoTJVidmNGK2a0ghpoDlmyWfFTFjvdR5RzfCbDbrRZLRlXQVikdnwwsZ8xqQWEQXo4ZY6a17woz6G3MxoDt2kBhVxLEUNJFCobQHkvn24IhgkN6+LPOu3TCtrMLXrXjBIOosj/rE3znNJx/22KPiw40RUHe7eNGY6uUwBJeO2h2eFKMDAkGhvcErVTQ8cWOjHGy4DPwZlms4Ng7qOJ3aVBj2NLYqiDfkkNO/zQE00uCgm1UfNpkG7AYbnyJwflsTKFIBptvLsfyzU8uVY9ZoCSw7M02UbbRkl3hDzsbgywYzQVx7yApSbmC/zrf8EZbrCzhQPZ9skMUgMUfAYAwGAd3NhoVQpXzzT7d4SH8v52NEIYPsgELWibZUcoWSPCJQdDvQWDFyKOeUSlNlOazvnlgEwUw8Wu2UvSXoMaO2KdZ2ICn77//Hm/hyB+sjUDGv2gPtvNvFho4LLAZVfCDMcneD5v2OMdK0vMktjQH2xvZJMJWl7UdOnSIgfL8+fPt7W282NrawgvAiCJWbDjsyJEjPT09fX19+HIciWNwAN7CaxtbPPYOvAR6XoU5+Q7RcZqN2rOiZZ5Ow6KS9DzLXfpSux3ZsZsjPLGhOnz4MIFpY2NjZWVlaWlpdXV1eXl5fX0dO8nZEVDIdeJvURRA1dGjRycmJsbGxgAyHAN4YT8dSR+xLZxtYIL03FGwlLfaZvd0sXQCMR62dziWEI758VeWUiXoEQQj283xC8YW8ESvAaO5ubnZ2VlAanNzkxycDWtmXYQqvAZunj59iuNnZmZOnTr10UcfAVvYj8+ybbMJGW3MwOgMbQQEBz5ogYJONsa9fEu518h7UFBojmM55Mn2egyptbU1gOnhw4eLi4uwVbjpsDdkwHyzR5iwd5JxwgtgC+bt/fffP3PmDFAFbwh4kQkkXs/mBPuNVfLA4BDyPDKFD5InB1I+x981wIqJ6f7ACy+CntF5S0YhmxPeb1MoBg3+Agd37tyZnp5+9uwZHYZ3hTSiM5zO2YJvAaNXr1598uTJu+++CxeJX4FbJAw5dJ6CUNpPoagdSAYNfFDBsoHuEHn6Wsf5JmOsTrdYAsFSFikkZU+Zttv+CwNsEymgB/5ramrq7t27cHk4pru7mzWC0tcLswQ6jy+5ceMGvnlycvLs2bNDQ0Nk0shuwVaRHoGNCD4xfcKTDbuYwu54xiDinS/xfWKj3rBoDkyCoBBzgjJbj30kCET2egQmUhMwuhh1oArG6dq1azdv3kSIhz1kwPApDHlQPs24oUXBbhTfduvWLdhCAAukfmRkpLe3FzYMf0n0AqDJ1dLvEs1nodWWynwoMFBkcGgOC8pgHWqxkq4wiTajrtELGi2boROqACAMKobw9u3b33zzzcLCAig2xpipNB1Z5eYy/bJPHl84Pz8P9nZ4Z0PwCJwNDAycOHHi2LFj2ANbhd/FiTF08CX0b1Jo4F8UktxJJaIJ01U0iqpYHYEtZ9txjVI7MKmcNH0tCZswHtiJ8QOMACYYqvv37+OA/v5+itpYfWBaVhFbNNj0hayQgXvhX/wizgE4o53Hjx8/ubMBYcA9zCeNOisaeEHn5qNH8Gt8DkINKr/bkDesAVix5yNokPyddiZOqKaSseV8ITsj9n14AZf05ZdfIuLDGDPykgJsuYeKww7nqwjrnP8BwhCKkgB24cIFWDKMN+AFG0Z0jcNJltodHBBKgu/yjQ2K+3SXYuaqOtoOfvjhh42m/BxZMomzoJWSGZgvox/a2Ug9B4zw4uuvv/7nP/+JQcK/WdA5kLNl3TE6VYAeuHn06NHMzAyGEzaMBQ7OVzq2R1+aoVQQO9RiCWzdGRgBWILyrmFg/NSSSaD9GCHACDz9H//4BygOkXRHh9REsqWjYLnIjk6Ds5A4MaAfbvqDDz4A0wevp4/DeuHMWQOzOZyTBbeV99LUsC4uX7/FcmyVTafkB90WMGOb/a4d9NnRX7GzYTC6u7uBqj/96U+wByRZ8c2Sf6W0EQrW3sS+is6BRSzas7a2Bmzh5MfHx5la2TKVRjc26kJtp4ynxsx00YQfTEoJQUaf/KBwB50sDTYQFzCYq1evLi8vA2E2t2B6oTFRNRqwII8hSDFVovAQXnt9ff3KlSusb7HFipW9OxSKowdnv8/DnH+ryHhVgeWXbCeJUZIemVDRprLInRwKaUiUokHEt7i4+PnnnyMEI0FB47XbsvnnRv76m2++wbVcunQJBxAPIwFCEDZ5P+v4AvIchYz/daBWmsvXKTcE+bswhEFbJYjyMQGCzRU8CF709PQsLS395S9/WVlZweuKAU4W7OoK3ek5+de//oUreu+992jUGSuOUQlO4wnmsGNAbELNKqoDSNCcgq8FRu9HjjGYMnWzy6fwrMPrbWxs/PWvf4XFAqooAexY+HaZKP3EMjI5MFTA1uDg4Ntvv+1P1ojJm0GXJ5i3IDOrzuWLWm6Txs1pgOXAK2n/bG2dk8fXrl0DW6eiKIqnZN7QHMfKsmfOpZEUt729Db41MDDQ19dnZ6+pkJALcoL5GSNWlvo8PTjnp7Ql66r+FGpmxejjJoHgO76PUXjw1QYw3dzZSOamtF0yAExGdlWiwtKfJSuLS0BgS3yLi6dNpChI0BE11149KM62WFmVMEnhKimQBi2Tb88IUlwDAyc4NzcHc0VMK1iVW7uJam46KFsjXOO9e/fOnTs3NjbGJc7GKwy0z4cIuD9RNhg8CtTe/p5cKtlV0QnKgq+gNQiBpJwRcmwVGSSiU/AaIFicM+nwLWk4+fnZ2tqamprif8njBx+/2DPpGzYnRVu7Ll9kIUmprWsEd8Fu2RxL+CAnj0FyYaWuX7++sLCA11w9F7y/yVxHlhGq8lkl5cJhuDqwxtXVVTw/JKjaYm8WPU9yLyWXT362KGGoYtnAJIBiz5AmUejEjEzbKS04Pz8PYNGj7CfXqtjjKj1C9Gw9iQ8wrcePH1+8eJHnmSWbhfgKmcBtgpcmcPkktorS911TWyxzcI0QH/tO1kXJG96+fXt9fR0IM69PyKkFPQ3FjBqDwWOJy3zy5MlPfvITKldksTT4PQJu/LItza+b/NnVRdYdjDm7LK+nF+Jjeqk9JwJ3GT7iu+++o3IGI/a+CnrG5vSFiuTdOaWVlRWezMi0OneiWFBY518JylfJpyKI46LccxkbcnsWgBDZGbHdnpzDYeGK7BOAReZKrocpncOpwpOqfJVfyIVr3N7eZr3XRphQUxWbruObIn+idkzTN16rHP/Xi9JOUNBUZLtl1PN2gsez1E7Y2tzcvHPnjhMJJqHTGrNUhXL5rbYQG+JiBwcHuTC/hNQkqA9JhhDs4xVLipd0hTLTMrravWC0LIeH9swIszPXanZ2Fj6CiuOU96g19Ksikvx3sdHUDyDMIRUVq9eVHSuDkGokpWNPCLZ3Og5RoPb65I/z00Tb4RFmZmb8H5VhIWin9TrKcuYtmGyhvwCWMy1bngFmIrlngcsHnZ1/hlVdYbKZp0YXkMV0OboMFgPSHrAr0A6amGC8Gpskf2/CRFWxZ8EidH5BMxBptoVd2MizLZKTUYN+Tf4gW41gFVdstmMaWMKcPpMqxNNPghA+EiuTZwVrbm4O2LJr90r0IG0XeRfqWIICFc2qEFSY3GSU8niNeJFhseSpVzE+HosZldiSgei39FhcXLQn28ipgtYT+RJdsgX1iObtCAWSwanPAj6CrC7YmiYWV2brWDGXV24ujRHLZpJivXm9fJ5RhSd4YWHBp1b6ZqQdRd6FkWP7zaKATxIEnb3iwxBbJ8YXIxLAslUNjS7g/KQzJdWIaWmbltlpUQdnfjQAgrWxsbG8vByjVvKCOXokNVePldtgjYIVQWfW1Pcl8zkx2uQLYLEO4SqLpdQdYiVBMTyZeLW7PLo2cweqqLlZrrmqEUk1JgeD8mNQSvCBFQxygzbMDwuc41l9FbqJmHhHLt5ZaO6dhlMbRV2DqVb74LRjAG3//vvvaeqBX84gwKtlcV9p4i93Sk7W4mXN4hI6hzvfllyBxr6iDIHUJonCvHi5RYwR28gk+7DZNGt1dZWKj/VrUrRMiyoRMwqqo5ywj/UaTeIsWZ3sdwILqlb+R16U78r+TiglS0rtQgZa/sLYZ22LhVOHxXJQbhSV8o3irHqdjFBvHizN0+SJ9abLT/vKi1/EIo8XXEWTxvHVXhkiAmFKFvrJ/pGrZXD2a2trQXafy5xaNpmioumyc892Blr+BmEyhZ+icYbJbshrQh0DBWGikIWGEnK2XGWVtUqgUJ9DnbFjbfiruLDkom0N2TMhbWKsJVWCD6FQZRXk8rE6BUGq1WTGohxLOdU9JpaW1uX1MyzsVAYVUhpdL4PSUWHTU+zlg4M1LXaJbLLZmpzpkz2mbSP9uFWWtQoBVcI8oaBBElo56v+NTY92wlKYq+fPn9tqWRUc1GWTaqyTCdoGnpnjW6xa2i7EKhpiVs3EKyMKAVVG3ZY9OXdZ04o9meS237LnFGhskj7r3K46maS7kYOe4Io9vhAlGDk5Bow19I6VORTKZLNAd4LU3ij6guh9nxMVUksWOyMbE4WNuu9yp6lcfpxFreGDOlZsAqDsJW0j5+e8bZxxNknwnunSZA2P9j2gUgnThGCCzMEbtfXRNIUSYp/anV0tpEpgx7TmAJ4rykYHA0M5CS1bRLl2XlhL0RfiC/lBlwtHNeVZygkXms9yUbJ5fbkYjTVKdthqgU3K9X3+o/X9zsZz7RlVQZsUbN+t/93g2ogxF+GjrXD8q6biJTnXWahiqAIse8E34liHDh0KTqzzs7N1CfFNT8AXVjGhWj/qHEmQ4lURhRXqlCv5OsKViXRHMvE5Gs597or5qaygPWm6lAvH6SNEITaRV76sse9FK8VSekHMUr4ncuAivyX0NhKGPvjrhbJSyqTmCcpSQswa+fU5wUyO8/jSs2ten5uqWdxQk+FpS817UuOlesbt7e2NjY3BwUGyWEw07WmGTjGxY7R8SYJsXrAE126kq78V9KKIufNYPZZgioLCvWyWkibQx4rT/15ZyFBaL624mEAJdxn8RVpAhVa9m5iYoFWf7JoO5eSZmDRqF9f7bWqScUZg6Ms9ysnGMkJCXsBxrJeV0XVKKoGqWK5Tjklr77gsl7EznYKdnpmZwV/uUmGvgVDxrGRpVFOAZP9bJPN6wkokRlFzLDcXTR6pmaGvkaw6UCCNaQSxZcnJPt29e/fatWuXL1+mCJH9YKwXpj7JE1wMUSDvwY8ELFYSQ+XMgNEVlMqs8IBihfpaFiyRjVmuMdAbsOQ5U3qUouDp6WlKZzn130KqV7AFJQJhjdkrnNUinVNkSDpTBYOYCC4goyk3dXLJyTUH/HMWtFZZfc2lXMnh1/Cn3D799oKX1FVgfX19YGCANBe7ixUzpJhWbiKVfY7Z02hgdsTgn2qX4FOFW+xUkOYyaKcmR/lYHAit8ay0YcnmM02oD8J83WRU5QyKvRgTPCBiQ0fYiwnrMRcUbP9pP9LJ8xQuM1rod0CxnGk5g6ks9NPU1cixT+lq9+bW0jGpplNCLxf/g7xiiiBV+BOfYl9orIn8tmWNdZtJSvZFMp2nyd7EfE0yr5wFLGXxVhXrVa/UHkRVsDuekIMLgu+AtdC18ZYU8L2Vr9fEUpM2kuzCryAWhQvpSgoBsZqK0kFpMpzUOJQsO5QkVS12heUQbIeHtOSiEWuWZFcTa+iQK/vF6GyRLOWLUWkTKcSz14yQZYggKzdip2R+bujOJvsAZEkSJch7jWuExEyCIzpg6+7u7uvro9yO49p4dknMhjnFW2SQePlW463TZIv4sflhQcPcJROs0uqDnJDW5J5klZUUQqFgQY+qGi1W8quEJ0E+0k5/AVW0QoLgTJJ3wJkfYVJTfJNt1Z2Du4yilV7M9Cmf9dyIL3mLjTi7Xy90JcOLLMm+BCizeCEZEoSER48epb6YyZKYciOrF5mFcSySuA6GoEJhlp7pyzPGZEGfZxf68Mq6NRXJe0NSuwktmsqrfw0PD/skyVmQPKY+mEiV+oH4+mGxyfUyl6/qCrPMVRLmghYva8dKuUGwgq1xheUyszYEQbDGxsaYYJlIiWzyDsSWsclNt8T2FMmKhqBRDTYRkGeCB8UCOevMa0/YU1XpRW9vb09Pz9ramnm1up8Gu8k8em6xQ/LdclK7r2DR3YAfPHbs2NDQEDfy86m9Uy4cE7qCSkSSnuuDlS6NCqx0JUlnVyLyCv4iZTZomWd/pls5KSu3cj8rOZhbNuIfQJ4OVzo6Omozd+UNVCo1WVxeLsPs0jw3RteMr3R+xmQ2OablZQAsXr9Pj6R6g8SKYWByDQ6nBxXgdebMGb1biGUJ9SRH3xbKzQ4JT7BTvZlse1yOref2CCGLVRQFnII9gdOkWu7qGxHqqxuUQd8BcYXiZBtpwgfc34kTJ06ePEnVs0m65siKPlI1s/00FxI8mS5N1KP0HVk/X8IV2mcFXoXgiG+cvn+fqdAgpIQrLOEEwyXkO+vnTExM0AJguTFB0kRp5mNmcZ7CeP0dNfOGSwyAJlEoGwkbWLjL4+PjuMvb29vU1E9z8XIQqix5dSiwRkoISgDyQt+25E21o7jYCxcu4MKZALBNkguU7WArGXPo6wxkIt+lDImFSXyaKaxZarvGiuD+joyM0DJrWWxdeXAV9UF+xJVSsPMWnh+g6vjx48EV8zRquBHn8An54qQGG1XeNaJw6ZSObGxzdRQHW5cvXwbToqWLslI6WWY/qyJeE1/nZtBxpX19fT/72c+C02xiTrDKjJVkYJgcKVVUmKSfStd7QFwYTPNZe8MtHhwc/OCDD8yrVd0FKd8o2pnUUtoge3mj64jhXymCwdHRUVym3FVG2RNPGZAFAyPZxr8WFZZ2hbHz06cq5SsUHheqyoWtwnN8/vx5amoQu4pgb/CsoL0KzpT0PPYpXNrAwMCVK1eMN2E1K6WR5QqVFiuhvMeeY40L0ETssQ8qY/ug9sH9ouAEf/7znz9+/HhjY4Ma3SbX7c2aSVwFT06DCV8dNOKqIdT8A/vff//9oaGhra0tbgfi9CN1+rM5pevBZIAJNR3hbKNz5g73j/WefG3SbNNzzJWqlfycBY+h4Ai3e3h4mByi3eoo2bSuoVk9Gg6qmYtGTxGuDgHKe++9h6jQ73KrCVBi5yMU+mUF/jENsquVNzdXlxcSeTz5iW7QT3/6UzzT8IxMtkqz9UYvX5DafY+By8Ez88knn3BdQzLnqMFZsLtsjZX+/xNIW3NzD6TW6k0yfScLThaL2kYiaDp37hwL8Q7rbJSta65aIKBCAISrAKoo7A1mrvRzM2P3NvZtWZnT4L9drYRUrLJZKY04b1ERKeX5wWonJydpdczYhLjkJLtGLVZS2nb6LuFpgRm+cOECXtjmyikUzk3/afqH5SYwgiSnKxYGa2bNm0gveGfKmwlNzaioRnKVCH0Kz/T4+Pjly5dB4W16G7RM7EaTbXwbMtjCsDF6jhw5AuJI0a6jszull0JWTTmjTgjLTHy9SPmSG7dYFZl+lvrwy1/+Eo/45uZmlqjdOdfLrBF4euedd8AaudVqFQpbIlVQfessYMkKcrIkEvt/+9vfwnTRErex57i9lybLthTtHjp06Pz58/hrIou2ZYk4WadX17yjjgOWMip0IEV6NGLy7u7uX//61/gbbCjQOUYr6Iz4PPv7+ynINeoVTUsHd9Wjwt3hCvXZSV/0IycCFn/x4kVbi1dqPC12f0FBlf7t6enp7e21qxuSX9t0g7hsYMn8LnZJsVp1ZTWf3+lP3/TMiRWMV+YK0wVggfySBO8HEG3vQRpLCFK7GCrccJYbce6esza2skgpydz986QoO/gr8kB3qCsst9FTi4EZHh4eGxvjWoBd0cGW2vOBqhdFcerUqd0+FnsBWAdeX9QPYIIfOXPmjL/yaidfAqEfNBHs6uTJk7vitPc4sBySwfWlNF14F508+fSzZ8/K9cf7wGrP8JAcPzg4ODAwQC0Va2zd0dxG+SjEs+fOndst51wVWE43RyGmqOV26IvlBSaOEaJC3s7hWMlOOzC0cIJvvfUWJwe5IYw9LyFLyxDGK9ngrsqA7imLxRssFtgVgLUraLuxVNC9Ya72LLC4eQY1G+58eFFhO9z36dOn/Vm4+8DqoNCdRIf+/v7dwoLxACCSPXr0KHjhbjG0bxywiAtjkOANTa1995o726Io4AeNOoez94HVXHmTXYGUda/peAzV5OQklY13+DDAvuIZINruXPIutV572WLRrKne3t5k5UnbGSFOdWhoCE9C7iO0D6zWxVY8NkSzLl68CDPQgc+9vfIbSOH4+DgBa1f47jcIWPZgUDYXhgpE+NKlS/AyHWu0yFwdPnx4ZGSE97xB5L2TnyFhUVAAC0br/PnzHQssouoAVsxlC61Edj2wbKPdSo8meDpnM17RFQvWGC0Ai6poOsdTOzPLASya4+V0WvMvUzMEcrhjJ1Xtr439WyJ4MnsyV+jcAgALvPjYsWN2x/2O4lhmZ9Jzd3c3BbC7Pf28d4DFM3aCOgXGqa+vD8CiZSM7x5s4Mg3PSt0n750bFToGH8MGo9VpA8azuAj9PNPrjRBIk366Frpdzovr2Qy2/v5+e6nIThg5+xxoHV6HZpUgAM5z5XCmVl5dIayp50RYMX5g4qswGq8hR9LaxxY9izm+YE9Ex2g5NSTUZUq+wBbgyT7J7Z2NVp6yY8MglZapvf+vv78EauXIybcOneUKgzypevgNQ0W8mIat01Qi6v+xtLQkTHEOgrKWuKEhhtCJHCvIk5w99kpoyftCzWGzZlM1Hbc6PgGnNzs7e+jQIca98Pw4zq7i0+t8W11PXVdnQio4BvYe39/JJuHZs2dkriiH2DlqCG93795dXl4WFnj2Q5PSlsb3YrXbra7WoKTeMRBsuK1UkUba29v79OnTBw8e+P1U2/LABDfQ9vn5eWDLngCijBBbH4hoRrawWxX6vDjm12PLVsceNRNaPDj3Yvy1zuzv55OnuA9DBc+Cv48ePfryyy/xF2PGK9LI5q0hnm4iqrd5lSrAeZ4+fZocorGmsCb5UJK5Cw9tkHUIoWWwkaT/blEvioV+m44X9wNPG9ma5az9fvY8YZfaaQBM169fn56ehh/s7u62l842Lcz1+qMbHEvgHkbr6tWrv/nNb4qi4Ngw5riDwaAs3/hQlvlWLm95zWLVde/kFcl8rASXPfYbqsaODDJ08ilAFSD19ddfT01NgRQfPnyYbBUJRVT1YC/I1hZU+Y8Nzgqn+u9//xvn/MknnwwODuKEt7a2KPJIhs8x0hkMF4SoMJdvxY4s6rp3MR/nLNboL1udZaKEHsPYDwAhbv9qZ1tfX+/e2ehIAhO7Fdn4NaGeCCIWnQxN2sYJ37hxY25u7t133z1//jyVwMuKKBu2oNEKatEaK6XndkHrePAXv/hFsINbuT4TyQVzTKQpvNy7wkS6y/FsR7wASV9aWvrjH/+Ih548i/8pUrOEvq51UXt5hIJPBblpOr3NzU0QeQQcsFsjIyMwXbGQ0NFfkrCIHSY7wSAtk83biybpyb7yJtJPUdPnXWhOYnSLmgb7sHHzE6JTQNW9e/c+/fTT2dlZ0q+NumF/E1GVbAaEUeTFVHFdy8vLMzMzPT09J0+e5K5M5MqTunyQjckeUIgwhMkHMfX/JbBi3TidVXTldr/KdciTcIyZNG7cQ1yKXwNJQNUf/vCHhYUFDAPGIChhV18kojSqNGGdM5xAD9wimS5c4KlTp+ATnbbQTowW/C0fE1z0kdS0lKnGIKvLK/TTEIgkSSz3Vb7jIFQBSXisf//736+urlJJk71gk0xRmzBXysAqZmA4N4CrwBNC9Ymff/45wlt7qeKY3Q3yHk0mt/ZbVMg3iMfPkbz92M0n1349ZIx9K7m8f5K41zdv3oStojHg7yGS7tNzYa2R5opVZKMV/FFuGYfnBD4REcmf//xnXOzk5OTGxkbMDSWtiyBPmNBkO5mey9sLVxhzCrHO20GSJCx2pV84yu9gzk6QlALmH1RyiecYbB3DQFSdy5GD2o+z/kATimgW501Sfi6txvXCJz569GhiYoJK4+3iH2cBHKEGgT0ph5AxxAhFOEFf6afauuQrdExO6aynpiQhiD+CFBeT0Gv6Fw7is88+w2taYdXPHGjYjD6tlluGr0GV7LlIeaeLxZMDXw/bTMocS/NG3e0neVuqPzD264Mff/xxMI4z4koYcoP5GHM3inXb7XaXDCOyUrQHj+z9+/dhqG7dukWVcQAWmyhhoRSTufKCJkTV0NgkrdRUtpG6izhxcXHx7NmzNAOR9tvWS4Z+EtlBghjzjEJk8D+5Qeh5KstORlwNwcRXOrC7PfnrWdgekA4DsPCkrqysfPHFF3//+9/xgsRPRpW8pogQBpZeCL0JIT4WJPKTNj8/v7W1dfr0aTsxmhuyJY1oLJ8onL9zzkUwG2P3IXaIuRPZOnw8uGqZz75thm6jikUEe2m4vr4+HIwn9caNG1NTU2AbOAwg48K9oKQek9djXF7pFOQFt5NSe5Yr9PPrNAPxP//5DwzVr371K/jH9fV1ulekIMSy7LLcIEtuSgPmvC5i9zqWqhOGwa9FthvS+fbPkTrtUiRK+eHGkZAzPT0N9weSQXbrfyrcjj1jTqpkGzUGgHpgyeZKSQ3pAnEHvv32W0op9vf3r62tOY+9sTqT851J1jebVCGyXg9yqxuUSVkffDEbEKx/4iV+bEjRa/g1mroJO4RnEUHfnTt3njx5QmaJe+rzvXNQG1ND5Ey5vG5FFQgqP6sU2+znlnSWp0+fXrlyhfpDb29vE4yI1/MjrekPWLr6LU/H0vg++9GPLR/lLAlmv+Z6KbpsolBUK7K0tPT48WPYp7m5OfJ6eJfiPuZSMejEMt+52pWeV8kWS6kh50KNENPT04MbhSBmYmIC2HrrrbcovmG3SJEN3zTnCUzGuTH3F8tqOwcXyYqX2NgElSo7BeQsZ8DMibBCcKF7AcYACgXL9ODBg4WFBdh2+91gtW4M3BqOVYWPV+mkIBiGZNTm457UO9y6//73v7Beg4ODb7/9NkA2PDxMpn1jY4OCR84zsmoj9K7VXGCywszlWP6cKlbenZVL/SoD+y9tLGna/g6XSrYK34wrB5JgmfDkPXv2DPaJLBlr6OQuHbVGZtyCa46Zqyz+niU3VHF/SXZMdwZ/qSk8CChIPcgDEDY+Pg6QjY6O4i3KMBprLXF7Hopvs+0lxwVzlTRgRawaM1iXF8sK24ITL3PK/xKY6BrAnFZWVmCWYJ/g9fAvgYmx6Iw9zYvXwyWXrVck8lkGLEuID4LJPphqafj86T4DE7iruLcA2YkTJyYnJ0+dOnX8+HG8RVMX7Z+wFXw/SHQariRnNToHFDKxtRVwdm08/IQhfsvsLKdLMGJzhbOHZaKrfbqzbW1tUf9Wcna+xB+ToEo7MuHqgnvq4vINqQ+x+OOlqdi5q7jt8/PzcAVAGOjX2bNnx8bGKB2Em0/OkYwTpe35dfBJK3HVhZ34dJLnjhezvdvBVxtTb8rf0fnhyYApAoaWl5eBJ7wGmOik6Rt4LoofKGTRc8H3Bf17EGTNrWaob1CQJcQnRS9i97jJFCQiEpqZmQGqiOYPDQ3BRWI/L0VLQ2OLYfRtHGAKRacxcb+wuxXaFMpm2TZDYmfHoRyVk8PHA0lwc/iL1+BMFADTkuD8WTa8znLtvtAanETvD7/MnIKarcyxmtYXhESbEWdTalJG7NSoMsJWHPB4f/vtt2D68IynT58GCQMbMzu96TB8NKGXPIzj75wkt5ZjBZUCRo/t6YpXG/kv4B3WCBgCYYJlQijHs43pg1Q/FEtFBxeK1Xs6jYCuDANbuf6qhoRV/E47ycNEgpRkKqvHeCFggtECtkDCRkZG+vr6MHZkwDhyJFXMVgqVhcuvWSxjFcUSSSIDw2E/7cTPk84Es8TUmx8LOnv7PMjGBp1a0GgJdVpGrPsTTJqJTE1rYoqOUtbKDQOTls852I6jnenjNFLwkhjKqamp27dv9/f3g+DDhsFFkn4BV0O83m51wXaLai7kviCu8s4/zJapu7sbe2CNHjx48OjRI+AJrzlSI/wJVEDIFfpZCE1YGjwsqWMJtUAaV6hfhaqWd7NgJ/DOGDdg5kP5VpAWEHy4SJguwAtMf2BgANChmWe2FXRqo+XKqMI2oYwVbIAUADQ9Pf3w4UMYT/wM/mUVyhbc/KRNTBiTk48+yxaSmPI9FdI4mhXha/FQ+iRJRSFeI3rJzwxZBwz07OwsOD7iR3KRcJcAHxkwGn27V4+d7fYpV2ELCsSN8HkqJL9+/TpCVkre4V2yXv5CZ7KxCZooeURj5krIwwRLpY23+KqGQjVH3mXQ6Nm6htrrBTYO2ki1BtO6devWd999B2oP6zU+Pj48PIy3yEVSTMClYDalcVM6vl4FJMFEffXVVxsbG6B1jmLLESJ701hXIN/15PLxLAJemjBlCVdZv1J9PnGJVHGursbkiSvkqEJ1cXERZgUgA8JOnjw5MTEBNkbwIjD5Cr6b0uEJVSyRw+NSvwMydzzvxTGDwZSTkjZpxMygWdI7NX3xT4vJu0Yv1cxJzJLvg8ezvXHSOOS+iKcjVgO9vnnzJqzXO++8A5zxrH87C2le1VLTnoMffvghO0GqdgKkbty44fs7p/TPxCdWxHbGpiIa9XwN33LYL5zUk4nMH5RJVctS1PqJh1mdYJPU3kRmycaiUbI1QBIM2L179/Aa9IuIv3m9uYg9zf/gRx99RNkVAhbYFULQ+/fvk7CZxIqJ1wTLhwmTgoxunrQ86bRDapFLV9Hoix2yqL1S1AgijxKOpA+A4w8NDXExPpXQ2YpDwZyJO7EgNNBIPoIiIPRDk6l9jVJ7Uoivha1Xz6nps4dKOydnrzUKvhBdEVQArCdPnly4cOHSpUtcwspLl5Hdeq15MOwbzN3y8rI9mTiY9K6rViTrs8oJTMl3q6zkkYsz5Q/VpchXp/bCbeSaepgrIARE/LPPPltaWjp69ChZJa5qeekK2Q+CrYOjAYxcvxDzYsEydsGd6WlW9c3X3OUvl4c/K5OYdCgCrDV990xOuxGNxfJ32vzHb01jux2ajvbw4cOBgQHq5mWnGrtstX1zcxMWS8/4BHtgxGUBZBueNetSmFhS2pAke5qXsEm55k0or6vI1oW5hPoQhNewXV9f/+KLLxYXF2G3bLHzNWCtrq6yH8ydt19X+2h9BzD95Ozmhr9lnw1+TxW2nrz/scn1tm2jChfYo7/97W8AD4WKL3PNdjkDzBV1CggqlkkHJCiiJZT3LC7vy5j67gxZ3q2KZ6wuimrKZoyuC5c+MhV8NPk+IAn26Nq1ax9//DFeP3/+vMuZOQN2ZepbdyX30cw9uMq7dXm3Rm1hE7OySt9wGdkwRnfu3Hnw4MGRI0dIqCq4LH1jYwOeMmYD7PaKyRShRlPIMlr+ztzUSomuDSVGUdmvMUuU0tgS4YWTLc2qytfEAbYGiThxdHT0pdFigkUlDL7Q0AQ919zu3CYWnb9lqQ/6pm0yvGJ97WupCbNDRSDn6dOnQNFr9aFMsITMmsZP63GmsfZZ9FzD9Ev4rB9ztrrCwBIhc27zLXnslF1G7M9ST16wKZr9UHBXwqAuKojjsiuMKe9BLl+OnstCfBU5qnSGp/rC4FkpHU2fqixZSxnRxz4LLC0tLZGOWpARA8FCuMjN9XObDtRVqB47WP7pGic6N+0KK9pdffpFL3HlehIBsrBVa2trwNJLuQFIWllZgR3jPouU99HcjtxVlrMsh3KJouT85qwSq+Ym7ZT7V+8Zk1pxvQFmEHxUbvpiwgQBC0CT60LbIrXLB7RFIG2XkUveZH1FfG4ApGylTI80L9Hwcq4fXKNdQ6iZlSVXN2QRMuUM1Vome1V3WC3zjJrcg5KQ5bZd1XCs2GfhCvv7+1+o71tbWwgU/SUC5GtrgsvLtCmrTqaK/20IOiUS2Mqad1N2qn4uPTfi7EIS4mGkRkdHuwhY3PJG7yyqh6zKjqtGXJDDqNO0u9EVyn5fXjiu+jQyJdRscRhvwUgZ6uKBV8+fP6dmVPWGgUZciT5mS2LWS5gol3Tcpcl7jXnGGsm7HP0luzUrH8isy2EeNT8//6LDGf5fXV2t1+yXfiZyS3XLqZHVZcyWGb+snnoaIV5fcpgLO6LmlBt8+PDhC/e3vLxc4oLL0XPlRC65AaQvmWbNZs56LuvqtFYvedfr7FlCfBaN8fdzm5oXcyY2NzdJGi2HrXJSu8yaNVJ7VoParBF1PisveV9uPYHq5D22MLapo0NEuaiQup6SCPoiFtzc2XITF0rxI8ucygfnWua6XGHHknc5YKqOpCzVnjZK29CMnWJlZQVRITVPL2HJYzOhy3Wf8hm68K6Q8wl24BXMm77th2zP5HezLJZvKTXLIFYU4jUzfASOZayuggU4PK8BUfph0nAso+ieoFxeuoQKKheUNlRuWqMSq1/UpDQJq+gTnAfsJbDsdQlr4aFCO/gkx9LQJrkWuSIZar22bipXKldU4eta1J63gmYbNnFPZbpdwlxpUjq1j3dWzNicPcudU1kvWy+xvagi5VazFTGUXIIg2VJbPlI/uTnLFVbRJlpmzzS2pFzP3Kw6mQxg1dKMxX4OHMaW67OURwYr4sshw8eZ/qtkMaJRXT6rOt7kz3StarHsNdnq9YPJVW6Mep2VEu/WZYSakyeqzF+q0m2gNVdXVOHsGmxp9NLYkXWVM1S5m+0KErOSd3o6n8WxqmxdTXcOFnSUXOqgzPDvSYE0C14lgsTab05BhaP1lorLEwCDecbY0iOad/ddoSmV+8vq51bGFda44qhvchxXm9XuMXliNjpzV4JV/lsjVrKcnazaVwGWfjZ9JWCZhotxq/SlLTHpWSk4NfHB1qsPTdO+qq7Q6YbVglsWdL5Vuhdn+b4qy4m3bFBbLMTXDriiLbPwhLW+fEum6e+Y1b29oZbapslariZcYaOhTFHR4+hdYXLZEtOY1G4q11J3pivUS+1KplW/xWqNKxRWGzCRZWRNaDH32OI8nUaqlAQ8eVZZTUSUNqzpB6loixOsnap3Pqdu1OdmNfZtzdUVbR8qeY5hOSWi0Z7b1Ye/ac+YNcNn7wArxrE0CUSlitGyyRStMVE19hXbs8CS1/02kUWasoSxfVfYljCw/XKDz+Xl3IIfXiT7yZicAqx22aRaprMmgdWuILfNrlDmW3X5vpZNFWxZhWDnp9KLtiNJY9iSrSsbXd6yIbZeY2LR6Bame3MtlonXYwlTWOtFUpXWNJruBjVyLH9+Sts9YAdFhcFblrvofLvqZGos5Ut2JtZYqc6BV6uBpX9qZWpfb6lPLSfcgcDat1gZw6Yp7qs3zVy6UKI16kNnEvmOBlYuOSt3r5M2qaFKqeoCqdJitSVLVuw69AgVz01YrJZNoa5ledF9i1X+7muKZzqz5r00x9Lbqn1gtYJ8dPiZV++eVc7K7gOrPT6rZZMpGvKM+xarpSjswHVQmutNsg+s9hiwTtAI9p6JelMsVl0x4/72pgOrAx/xetPMHe773lCO1fnA2rdYe5ytd+b0r13nvt8UjtWWIdzbNmkfWPto2AdWhW23x3p7LFbt2n+29rcmtv8XYABtgMp03U8CcAAAAABJRU5ErkJggg=="/>


    <center>
	<h1 style="font-size: 65px;" class="cover-heading"><img height="150" width="150" alt="Take Photo" src="camera.png"/>Photo booth <small>#moot<script>document.write(new Date().getFullYear().toString().substr(2, 2));</script></small></h1>

	<div>
            <table border="0">
                <tr>
                    <td rowspan="4" style="padding: 0 10px 0 0">
			<div class="camera">
	                    <video id="video" autoplay style="width: 900px;"></video>
	                    <canvas id="canvas" style="display: none;" height="990" width="990"></canvas>
			    <span id="counter" class="counter"></span>
			</div>
                    </td>
		    <td><canvas id="photo1" height="160" width="180" style="border:1px solid #000000;"></canvas><canvas hidden id="tmp1" height="720" width="1280"></canvas></td>
                </tr>
                <tr>
                    <td><canvas id="photo2" height="160" width="180" style="border:1px solid #000000;"></canvas><canvas hidden id="tmp2" height="720" width="1280"></canvas></td>
                </tr>
                <tr>
                    <td><canvas id="photo3" height="160" width="180" style="border:1px solid #000000;"></canvas><canvas hidden id="tmp3" height="720" width="1280"></canvas></td>
                </tr>
                <tr>
                    <td><canvas id="photo4" height="160" width="180" style="border:1px solid #000000;"></canvas><canvas hidden id="tmp4" height="720" width="1280"></canvas></td>
                </tr>

            </table>
        </div>

        </br>
	<h1 id="note">
  	    Press the BUTTON to begin.
  	</h1>
	<p class="lead">
  	    On completion step outside to see your photos on the main screen.
  	</p>

    </center>

    <script>
    var p1 = document.getElementById("photo1").getContext("2d");
    var p2 = document.getElementById("photo2").getContext("2d"); 
    var p3 = document.getElementById("photo3").getContext("2d");
    var p4 = document.getElementById("photo4").getContext("2d");

    var p1full = document.getElementById("tmp1").getContext("2d");
    var p2full = document.getElementById("tmp2").getContext("2d");
    var p3full = document.getElementById("tmp3").getContext("2d");
    var p4full = document.getElementById("tmp4").getContext("2d");


    $(document).ready(function() {
	    var img = document.getElementById("profile");
	    p1.drawImage(img,0,0,180,160);
	    p2.drawImage(img,0,0,180,160);
	    p3.drawImage(img,0,0,180,160);
	    p4.drawImage(img,0,0,180,160);
    });

    var space = false;
    $(document).keyup(function(evt) {
        if (evt.keyCode == 32 ) {
	console.log(space);
            if (space == false) {
                space = true;
	        TakePhoto();
            setTimeout(function() {TakePhoto()}, 4000);
            setTimeout(function() {TakePhoto()}, 8000);
            setTimeout(function() {TakePhoto()}, 12000);
      	    }
	}
    });

    var photo_count = 0;
    function TakePhoto() {
	setTimeout(function() {$("#counter").html('3')}, 1000);console.log("3");
	setTimeout(function() {$("#counter").html('2')}, 2000);console.log("2");
	setTimeout(function() {$("#counter").html('1')}, 3000);console.log("1");
	setTimeout(function() 
	{
	    console.log("Taking photo...");
	    $("#counter").html('');
	    $("#video").fadeOut(100, function(){});
            $("#video").fadeIn(100, function(){});

	    photo_count = photo_count +1;

	    var video = document.getElementById('video');
	    switch (photo_count) {
	    case 1:
        document.getElementById('tmp1').width = (video.videoHeight * 2) + 30;  //base image
        document.getElementById('tmp1').height = (video.videoHeight * 2) + 30; //base image
        var crop = (video.videoWidth - video.videoHeight) / 2;
		p1full.drawImage(video,crop,0,video.videoHeight,video.videoHeight,10,10,video.videoHeight,video.videoHeight);
		p1.drawImage(video,0,0,180,160);
		break;
	    case 2:
        document.getElementById('tmp2').width = video.videoHeight;
        document.getElementById('tmp2').height = video.videoHeight;
		var crop = (video.videoWidth - video.videoHeight) / 2;
		p1full.drawImage(video,crop,0,video.videoHeight,video.videoHeight,video.videoHeight+20,10,video.videoHeight,video.videoHeight);
		p2.drawImage(video,0,0,180,160);
		break;
	    case 3:
        document.getElementById('tmp3').width = video.videoHeight;
        document.getElementById('tmp3').height = video.videoHeight;
		var crop = (video.videoWidth - video.videoHeight) / 2;
		p1full.drawImage(video,crop,0,video.videoHeight,video.videoHeight,10,video.videoHeight+20,video.videoHeight,video.videoHeight);
		p3.drawImage(video,0,0,180,160);
		break;
	    case 4:
        document.getElementById('tmp4').width = video.videoHeight;
        document.getElementById('tmp4').height = video.videoHeight;
		var crop = (video.videoWidth - video.videoHeight) / 2;
		p1full.drawImage(video,crop,0,video.videoHeight,video.videoHeight,video.videoHeight+20,video.videoHeight+20,video.videoHeight,video.videoHeight);
		p4.drawImage(video,0,0,180,160);
		break;
	    }	    

	    console.log("Done!");
	    setTimeout(function() {
		space = false;
                if (photo_count >= 4) {
			$.ajax({
			    type: "POST",
			    url: "upload.php",
			    data: {
				photo: encodeURIComponent(document.getElementById("tmp1").toDataURL("image/jpeg")),
			    },
			    contentType: "application/x-www-form-urlencoded;charset=UTF-8",
			    success: function(){
				console.log("uploaded");
				$("#note").html("Finished taking photos!");
				setTimeout(function() {window.location="https://localhost/";}, 5000);
			    },
			    error: function(){
				console.log("error uploading");
			    }
			});
                }
		console.log("Got photo: " + photo_count);
		$("#note").html("Photo "+ photo_count +" of 4");
	    }, 250);
	}, 
	4000);
    };

    window.addEventListener("DOMContentLoaded", function() {
	// Grab elements, create settings, etc.
	var canvas = document.getElementById("canvas"),
		context = canvas.getContext("2d"),
		video = document.getElementById("video"),
		//videoObj = { "video": {mandatory: {maxWidth: 1280, maxHeight: 720} } },
                videoObj = { "video":true },
		errBack = function(error) {
                        console.log("Video capture name: ", error.name);
                        console.log(error);
			console.log("Video capture code: ", error.code); 
		};

	// Put video listeners into place
        if(navigator.mozGetUserMedia){
                 console.log("getUserMedia supported");
                 navigator.mozGetUserMedia(videoObj,
                 function(stream) {
                    var url = window.URL || window.webkitURL;
                    video.src = url ? url.createObjectURL(stream) : stream;
                    video.play();
                 }, errBack
            );
        } else if(navigator.webkitGetUserMedia){
                 console.log("getUserMedia supported");
                 navigator.webkitGetUserMedia(videoObj,
                 function(stream) {
                    video.src = window.webkitURL.createObjectURL(stream);
		    video.play();
                 }, errBack
            );
        } else {
            console.log("getUserMedia not supported");
        }
    });

	
    //(function(seconds) {
    //var refresh,       
    //    intvrefresh = function() {
    //        clearInterval(refresh);
    //        refresh = setTimeout(function() {
    //           location.href = location.href;
    //        }, seconds * 1000);
    //   };

    //$(document).on('keypress click', function() { intvrefresh() });
    //intvrefresh();

    //}(60));
    </script>
</body>

</html>
